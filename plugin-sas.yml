---
- name: Install and run the Viya app
  hosts: localhost
  gather_facts: false
  become: true
  tasks:
    - name: Wait for the system to become available
      wait_for_connection:
        timeout: 600

    - name: Change user and group id
      script: change_user_group_id.sh

    - name: set the hostname in /etc/hosts
      lineinfile:
        path: /etc/hosts
        insertbefore: BOF
        line: '127.0.0.1   rsc-instance.novalocal rsc-instance'

    - name: Create a user in whoms home directory files will be placed
      user:
        name: automation
        comment: SAS automation user

    - name: Add automation to sudoers
      copy:
        content: 'automation ALL=(ALL) NOPASSWD: ALL'
        dest: /etc/sudoers.d/automation

    - name: Install epel-release
      yum:
        name: epel-release
        state: present

    - name: Install Ansible and Git
      yum:
        name:
          - ansible
          - git

    - name: Set SELinux to Permissive
      selinux:
        policy: targeted
        state: permissive

    - name: Make the sas directory
      file:
        path: /opt/sas
        state: directory
        recurse: yes
        mode: '0775'

    ### DOWNLOAD REPO FROM NFS SERVER ###

    - name: Create mount point for NFS share
      file:
        path: /mnt/sas_warehouse
        state: directory
        recurse: yes
        mode: '0755'

    # - name: Install NFS-Utils
    #   yum:
    #     name:
    #       nfs-utils

    - name: Mount NFS share
      mount:
        src: 145.100.59.95:/data
        path: /mnt/sas_warehouse
        state: mounted
        fstype: nfs

    # - name: Copy files to /opt/sas
    #   copy:
    #     src: /mnt/nfs/repos
    #     dest: /opt/sas/
    #     mode: '0775'

        # JSON 664

    ### DOWNLOAD REPO FROM SAS MIRRORMANAGER ###

    #
    # - name: Copy the SAS Mirrormanager
    #   copy:
    #     src: files/mirrormgr
    #     dest: /opt/sas/mirrormgr
    #     mode: '0775'
    #
    # - name: unzip and untar the file
    #   unarchive:
    #     remote_src: yes
    #     src: /opt/sas/mirrormgr
    #     dest: /opt/sas/
    #     mode: '0755'
    #
    - name: Copy the license file
      copy:
        src: files/SAS_Viya_deployment_data.zip
        dest: /opt/sas
        mode: '0644'
    #
    # - name: Mirror the repository to the local machine
    #   shell: ./mirrormgr mirror --deployment-data SAS_Viya_deployment_data.zip  --path . --platform x64-redhat-linux-6 --latest > mirrormgr.log
    #   args:
    #     chdir: /opt/sas
    #   register: mirror_repo
    #   until: mirror_repo is not failed
    #   retries: 5
    #
    #############################################

    - name: Execute the following tasks as the user automation
      block:
        - name: Create a .ssh directory
          file:
            path: /home/automation/.ssh
            state: directory
            recurse: yes
            mode: '0755'

        - name: Create a git directory
          file:
            path: /home/automation/git
            state: directory
            recurse: yes
            mode: '0775'

        - name: Clone the Viya Ark
          git:
            repo: https://github.com/sassoftware/viya-ark.git
            dest: /home/automation/git/viya-ark

        - name: Create a Viya directory
          file:
            path: /home/automation/viya
            state: directory
            recurse: yes
            mode: '0755'

        - name: Copy the SAS Orchestration tool
          copy:
            src: files/sas-orchestration-linux.tar
            dest: /home/automation/viya

        - name: unpack the archive
          unarchive:
            remote_src: yes
            src: /home/automation/viya/sas-orchestration-linux.tar
            dest: /home/automation/viya
      become: yes
      become_user: automation

    - name: execute the pre-install playbook
      shell: ansible-playbook -vvv --become -i pre-install.inventory.ini viya_pre_install_playbook.yml --skip-tags skipmemfail --skip-tags skipcoresfail --skip-tags skipstoragefail --skip-tags skipipsfail
      args:
        chdir: /home/automation/git/viya-ark/playbooks/pre-install-playbook

    - name: Copy the license to the Viya directory
      copy:
        remote_src: yes
        src: /opt/sas/SAS_Viya_deployment_data.zip
        dest: /home/automation/viya

    - name: Execute the following tasks as the user automation
      block:
        - name: Build the Ansible playbook and extract it
          shell: ./sas-orchestration build --input SAS_Viya_deployment_data.zip --platform redhat --architecture x64 --repository-warehouse /mnt/sas_warehouse
          args:
            chdir: /home/automation/viya

        - name: unarchive the SAS_Viya_playbook
          unarchive:
            remote_src: yes
            src: /home/automation/viya/SAS_Viya_playbook.tgz
            dest: /home/automation/viya/
            mode: '0751'

        - name: Copy the inventory file
          copy:
            remote_src: yes
            src: /home/automation/viya/sas_viya_playbook/samples/inventory_local.ini
            dest: /home/automation/viya/sas_viya_playbook/inventory.ini
            mode: '0644'
      become: yes
      become_user: automation

    - name: Copy the OpenLDAP archive to the right location
      copy:
        src: files/Openldap.tgz
        dest: /opt/sas/
        mode: '0644'

    - name: Unarchive the file
      unarchive:
        remote_src: yes
        src: /opt/sas/Openldap.tgz
        dest: /opt/sas/
        mode: '0775'

    - name: Change Root password for LDAP
      replace:
        path: /opt/sas/openldap/group_vars/all.yml
        regexp: 'Orion123'
        replace: 'HjjMYAi4Cvb4NmPi'

    - name: Change the DC
      replace:
        path: /opt/sas/openldap/group_vars/all.yml
        regexp: 'viyaldap'
        replace: 'surfldap'

    - name: Run the OpenLDAP setup
      shell: ansible-playbook gel.openldapsetup.yml
      args:
        chdir: /opt/sas/openldap

    - name: Copy the sitedefault.yml
      copy:
        src: files/sitedefault.yml
        dest: /home/automation/viya/sas_viya_playbook/roles/consul/files/sitedefault.yml
        mode: '0644'
        owner: automation
        group: automation

    #############################################
    - name: Edit /tmp/.root.ansible directory
      file:
        path: /tmp/.root.ansible
        state: directory
        mode: '0777'
    - name: Delete /tmp/.automation.ansible directory
      file:
        path: /tmp/.automation.ansible
        state: absent
        # mode: '0777'
    - name: Run the final playbook
      remote_user: automation
      shell: ansible-playbook -vvv site.yml
      args:
        chdir: /home/automation/viya/sas_viya_playbook
      become: no
      # become_user: automation

    #############################################
