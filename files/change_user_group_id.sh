#!/bin/bash

if [[ $(grep '1001' /etc/group) ]];
  then GROUP1="$(grep '1001' /etc/group | cut -f 1 -d ":")"; groupmod -g 10030 $GROUP1
fi

if [[ $(grep '1002' /etc/group) ]];
  then GROUP2="$(grep '1002' /etc/group | cut -f 1 -d ":")"; groupmod -g 10040 $GROUP2
fi

if [[ $(grep '1001' /etc/passwd) ]];
  then USER1="$(grep '1001' /etc/passwd | cut -f 1 -d ":")"; usermod -u 10050 $USER1
fi

if [[ $(grep '1002' /etc/passwd) ]];
  then USER2="$(grep '1002' /etc/passwd | cut -f 1 -d ":")"; usermod -u 10060 $USER2
fi
